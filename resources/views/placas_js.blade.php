<script>
    $(document).ready(function() {
        $('#consultarPlaca').click(function(){
            obtenerDatosPlaca();
        });
        $('#tablaPlaca').DataTable();
    });

    function obtenerDatosPlaca(){
        nroPlaca = $('#placa').val();
        // alert(nroPlaca);
        $.ajax({
            type: 'POST',
            url: "{{ route('/consultarPlaca') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                nroPlaca
            },
            dataType: 'json',
            success: function (data) {
                    var tabla = $('#tablaPlaca').DataTable();
                    console.log(data);
                    console.log(data['Description']['Description']);
                    tabla.row.add([
                        data['Make'],
                        data['Model'],
                        data['RegistrationYear'],
                        data['Use'],
                        data['VIN']
                    ]).draw(false);
            },
            error: function() {
                console.log('ERROR');
            }
        });
    }
</script>