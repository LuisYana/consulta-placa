<link rel="stylesheet" href="/css/bootstrap.css">
<script src="/js/bootstrap.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>

<div class="jumbotron">
    <h2 style="text-align: center;"><strong></strong></h2>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                CONSULTA DE PLACA VEHICULAR
            </div>
            <div class="card-body">
                <form class="form-inline">
                    <div class="form-group col-md-6">
                        <label for="placa">Ingrese la placa del auto a consultar: </label>
                        <input type="text" class="form-control" id="placa" name="placa">
                    </div>
                    <div class="form-group col-md-6">
                        <button type="button" class="btn btn-primary" id="consultarPlaca" name="consultarPlaca">Consultar placa</button>
                    </div>
                </form>
            </div>
            <div class="card-footer">DATOS OBTENIDOS</div>
            <div class="card-body">
                <table class="table" id="tablaPlaca">
                    <thead>
                        <tr style="text-align: center;">
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Año</th>
                            <th>Uso</th>
                            <th>Serie del motor</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@extends('placas_js')